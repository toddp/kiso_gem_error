class User < ApplicationRecord
  include Pay::Billable
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :pwned_password,
         :omniauthable, omniauth_providers: [:stripe_connect]
validates :email, presence: true



  def can_receive_payments?
    uid? && provider? && access_code? && publishable_key?
  end

end

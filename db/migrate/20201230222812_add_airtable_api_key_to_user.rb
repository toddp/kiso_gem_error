class AddAirtableApiKeyToUser < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :airtable_api_key, :string, default: nil
  end
end

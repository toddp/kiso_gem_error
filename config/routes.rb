Rails.application.routes.draw do
  root to: 'dashboard#index'
  get 'dashboard/index'
  get 'dashboard/import'
  devise_for :users, controllers: { omniauth_callbacks: "omniauth_callbacks" }

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
